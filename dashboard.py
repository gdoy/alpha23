from flask import Flask, render_template, request, flash, redirect
import sqlite3
import csv
import io
import requests

app = Flask(__name__)
#app.secret_key = 'supersecretkeychangemelater' Not necessary as yet

def get_traits(chrom, pos):
    endpoint = f"https://rest.ensembl.org/phenotype/region/homo_sapiens/{chrom}:{int(pos)-200}-{int(pos)+200}?content-type=application/json;feature_type=Variation"
    print(endpoint)
    response = requests.get(endpoint)
    
    if response.status_code == 200:
        phenotypes = response.json()
        # Check if phenotypes list is empty, if it is, return a placeholder message.
        if not phenotypes:
            return "No phenotypes detected"
        # If it is not empty, extract trait/phenotype descriptions and concatenate them.
        # Iterate through all items in the phenotypes list and for each item,
        # iterate through all items in its 'phenotype_associations' list,
        # then get the 'description' of each phenotype, avoiding those that contain "tumour" or "tumor".
        traits = set(
            [assoc.get('description') 
             for p in phenotypes 
             for assoc in p.get('phenotype_associations', []) 
             if assoc.get('description') and "tumour" not in assoc.get('description').lower() and "tumor" not in assoc.get('description').lower() and "specified" not in assoc.get('description').lower() and "HGMD" not in assoc.get('description').upper()]
        )
        # Check if traits is empty after filtering, if it is, return a placeholder message.
        return ", ".join(traits) if traits else "No phenotypes detected"
    else:
        return None


def fetch_gene_from_vep(chrom, pos, allele):
    # Define the VEP API endpoint and headers
    endpoint = f"https://rest.ensembl.org/vep/human/region/{chrom}:{pos}:{allele}"
    headers = {
        "Content-Type": "application/json"
    }
    # Make the GET request to the VEP API
    response = requests.get(endpoint, headers=headers)
    response_json = response.json()

    # Parse the response to get the gene name
    for trans_consequence in response_json[0]['transcript_consequences']:
        if 'gene_symbol' in trans_consequence:
            return trans_consequence['gene_symbol']
    return None

def get_maf(rsid):
    """Fetches the MAF for a given rsid."""
    endpoint = f"https://rest.ensembl.org/variation/human/{rsid}?content-type=application/json"
    response = requests.get(endpoint)
    print(endpoint) # debugging
    if response.status_code == 200:
        variant_data = response.json()
        return variant_data.get('MAF')
    return None

@app.route('/', methods=["GET", "POST"])
def home():
    chrom = request.form.get("chromosome")
    pos = request.form.get("position")
    data = []
    if chrom and pos:
        connection = sqlite3.connect('/largedata/share/George/AlphaMissense/db/hg19.db')
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM variants WHERE CHROM=? AND POS=?", ('chr' + chrom, pos))
        data = cursor.fetchall()
        connection.close()
    return render_template("home.html", data=data)

@app.route('/create-index', methods=["GET"])
def create_index():
    with sqlite3.connect('/largedata/share/George/AlphaMissense/db/hg19.db') as connection:
        cursor = connection.cursor()
        cursor.execute('CREATE INDEX IF NOT EXISTS idx_variants_chrom_pos ON variants(CHROM, POS)')
        message = 'Index created successfully!'
    return message

@app.route('/file-upload', methods=["POST"])
def file_upload():
    if 'file' not in request.files:
        flash('No file part')
        return redirect('/')
    
    file = request.files['file']
    if file.filename == '':
        flash('No selected file')
        return redirect('/')

    if file:
        data = []
        with sqlite3.connect('/largedata/share/George/AlphaMissense/db/hg19.db') as connection:
            cursor = connection.cursor()
            stream = io.StringIO(file.stream.read().decode("UTF8"), newline=None)
            csv_input = csv.reader(stream, delimiter='\t')
            for row in csv_input:
                # Skip commented lines
                if row[0].startswith('#'):
                    continue
                # Now unpack the useful columns and retrieve the data
                try:
                    rsid, chrom, pos, genotype = row
                    
                    cursor.execute("SELECT * FROM variants WHERE CHROM=? AND POS=? AND (ALT = ? OR ALT = ?)",
                                   ('chr' + chrom, pos, genotype[0], genotype[1]))
                    rows = cursor.fetchall()
                    if rows:
                        # Check if the gene name is already available
                        gene_name = [r[0] for r in cursor.execute("SELECT gene FROM variants WHERE CHROM=? AND POS=? AND (ALT = ? OR ALT = ?)",
                                                               ('chr' + chrom, pos, genotype[0], genotype[1]))][0]

                        if not gene_name:
                            # Make an HTTP request to get the gene associated with the variant (rsid)
                            response = requests.get(f"https://rest.ensembl.org/vep/human/id/{rsid}", headers={"Content-Type": "application/json"})
                            if response.status_code == 200:
                                vep_data = response.json()
                                for trans in vep_data[0].get('transcript_consequences', []):
                                    gene_name = trans.get('gene_symbol')
                                    if gene_name:
                                        break
                            else:
                                gene_name = "Unknown"
                            
                            # Update the gene name in the variants table
                            cursor.execute("UPDATE variants SET gene = ? WHERE CHROM=? AND POS=? AND (ALT = ? OR ALT = ?)",
                                           (gene_name, 'chr' + chrom, pos, genotype[0], genotype[1]))

                        # Check if rsid is already set for this variant
                        cursor.execute("SELECT rsid FROM variants WHERE CHROM=? AND POS=? AND (ALT = ? OR ALT = ?)",
                                    ('chr' + chrom, pos, genotype[0], genotype[1]))

                        existing_rsid = cursor.fetchone()

                        # If rsid is None or empty, update it.
                        if existing_rsid is None or existing_rsid[0] in [None, "", "None"]:
                            cursor.execute("UPDATE variants SET rsid = ? WHERE CHROM=? AND POS=? AND (ALT = ? OR ALT = ?)",
                                        (rsid, 'chr' + chrom, pos, genotype[0], genotype[1]))

                        # Check if MAF is already set for this variant
                        cursor.execute("SELECT MAF FROM variants WHERE CHROM=? AND POS=? AND (ALT = ? OR ALT = ?)",
                                    ('chr' + chrom, pos, genotype[0], genotype[1]))
                        
                        existing_maf = cursor.fetchone()
                        # If MAF is None or empty, update it.
                        if existing_maf is None or existing_maf[0] in [None, "", "None"]:
                            maf = get_maf(rsid)
                            if maf is not None:
                                cursor.execute("UPDATE variants SET MAF = ? WHERE CHROM=? AND POS=? AND (ALT = ? OR ALT = ?)",
                                            (maf, 'chr' + chrom, pos, genotype[0], genotype[1]))

                        # Check if traits data is already set for this variant
                        cursor.execute("SELECT traits FROM variants WHERE CHROM=? AND POS=? AND (ALT = ? OR ALT = ?)",
                                    ('chr' + chrom, pos, genotype[0], genotype[1]))
                        
                        existing_traits = cursor.fetchone()
                        # If traits data is None or empty, fetch it.
                        if existing_traits is None or existing_traits[0] in [None, ""]: 
                        #if existing_traits is None or existing_traits:     
                            # Fetch traits information using API
                            traits = get_traits('chr' + chrom, pos)
                            if traits:
                                cursor.execute("UPDATE variants SET traits = ? WHERE CHROM=? AND POS=? AND (ALT = ? OR ALT = ?)",
                                            (traits, 'chr' + chrom, pos, genotype[0], genotype[1]))

                        # Fetch the updated rows to display in the UI
                        cursor.execute("SELECT * FROM variants WHERE CHROM=? AND POS=? AND (ALT = ? OR ALT = ?)",
                                    ('chr' + chrom, pos, genotype[0], genotype[1]))
                        rows = cursor.fetchall()

                        for row in rows:
                            data.append(row)  # appending the returned data

                except IndexError:
                    # Skip invalid lines
                    continue
        return render_template("home.html", data=data)
    
    return render_template("home.html", data=[])
            
if __name__ == '__main__':
    app.run(host='127.0.0.1', port=2324)