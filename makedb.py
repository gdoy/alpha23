import sqlite3
import csv

# Connect to your sqlite3 database
connection = sqlite3.connect('db/hg19.db')
cursor = connection.cursor()

# Open TSV file
with open('hg19.tsv', 'r') as f:
    # Create a CSV reader for a tab-delimited file
    reader = csv.reader(f, delimiter='\t')

    # Loop over the rows in TSV file
    for row in reader:
        # Insert each row into database
        cursor.execute('''
            INSERT INTO variants VALUES (?,?,?,?,?,?,?,?,?,?)
        ''', row) 

# Commit changes and close the connection to the database
connection.commit()
connection.close()